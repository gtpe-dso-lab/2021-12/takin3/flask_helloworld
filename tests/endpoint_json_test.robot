*** Variables ***
###${URL}      ${Staging.URL}
${URL}      ${107.178.247.21}

*** Settings ***
### FIX: need the appropriate student's IP for staging environment
Library    String
Library    REST           ${URL}          ssl_verify=false

*** Test Cases ***
### FIX: expected response body
Test json endpoint
  GET       /json
  Output    response body
  Object    response body
### FIX: Go look at the returned value in hello_json in helloworld.py
###String    response body data	hello_world
  String    response body data	hello_georgia
