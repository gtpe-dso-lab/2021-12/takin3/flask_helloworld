*** Settings ***
Library    Selenium2Library
Suite Teardown  Close All Browsers

*** Variables ***
### FIX: need the appropriate student IP for staging environment.
### Go look in robot.yml.
###${URL}      ${Staging.URL}
${URL}      ${107.178.247.21}
${BROWSER}  Chrome

*** Test Cases ***
Test root endpoint
  ${chrome_options} =     Evaluate    sys.modules['selenium.webdriver'].ChromeOptions()    sys, selenium.webdriver
  Call Method    ${chrome_options}   add_argument    headless
  Call Method    ${chrome_options}   add_argument    disable-gpu
  Call Method    ${chrome_options}   add_argument    no-sandbox

  ${options}=     Call Method     ${chrome_options}    to_capabilities
  Open Browser    ${URL}          browser=${BROWSER}   desired_capabilities=${options}
  ### FIX: Go look at the time string in render_template in helloworld.py
  Selenium2Library.Wait Until Page Contains  Current Thyme in Atlanta


