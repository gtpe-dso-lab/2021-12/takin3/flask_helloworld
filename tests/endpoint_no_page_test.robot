*** Settings ***
Library    Selenium2Library
Suite Teardown  Close All Browsers

*** Variables ***
### FIX: need the appropriate student IP for staging environment.
### Go look in robot.yml.
###${URL}      ${Staging.URL}/test_page
${URL}      ${107.178.247.21}/test_page
${BROWSER}  Chrome

*** Test Cases ***
Test "no page" endpoint
  ${chrome_options} =     Evaluate    sys.modules['selenium.webdriver'].ChromeOptions()    sys, selenium.webdriver
  Call Method    ${chrome_options}   add_argument    headless
  Call Method    ${chrome_options}   add_argument    disable-gpu
  Call Method    ${chrome_options}   add_argument    no-sandbox

  ${options}=     Call Method     ${chrome_options}    to_capabilities
  Open Browser    ${URL}   browser=chrome  desired_capabilities=${options}
  ### FIX: Go look at make_response in other_page in helloworld.py
  ###Selenium2Library.Wait Until Page Contains  The page named test_page does not exist.
  Selenium2Library.Wait Until Page Contains  The page named 404 {test_page} does not exist?.
